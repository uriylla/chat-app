# Chat App

## Run Instructions

    $ git clone https://gitlab.com/uriylla/chat-app.git
    $ cd chat-app
    $ npm install

    // The chat data is served with json-server
    // Install json server

    $ npm install -g json-server

    // This will start serving the json file in http://localhost:3000/messages
    $ json-server --watch resources/chat.json

    // Finally to run the app:
    $ npm run start