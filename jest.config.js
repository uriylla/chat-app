
const {defaults} = require('jest-config');
module.exports = {
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  transform: {
    "^.+\\.(js|jsx)$": "babel-jest",
    "\\.svg$": "<rootDir>/fileTransformer.js",
    "\\.scss$": "<rootDir>/fileTransformer.js"
  }
};