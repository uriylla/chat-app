const timestampToDate = (timestamp) => {
  const date = new Date(parseInt(timestamp, 10));
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
}

function formatDate(timestamp) {
  const date = timestampToDate(timestamp);
  return date === timestampToDate(Date.now().toString())
    ? 'Today'
    : date;
}

export default {
  timestampToDate: formatDate,
};
