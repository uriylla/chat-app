import React from 'react';
import PropTypes from 'prop-types';
import SingleCheck from '../../resources/svg/single-check.svg';
import DoubleCheck from '../../resources/svg/double-check.svg';

const MessageStatus = ({ status }) => (
  <span className={`message__status message__status--${status}`}>
    {status === 'sent'
      ? <SingleCheck />
      : <DoubleCheck />}
  </span>
);

export const statusType = PropTypes.oneOf(['sent', 'read', 'received']);

MessageStatus.propTypes = {
  status: statusType.isRequired,
};

export default MessageStatus;
