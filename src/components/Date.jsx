import React from 'react';
import PropTypes from 'prop-types';
import utils from '../utils';
import { MessageType } from './Message';

const Date = ({ timestamp, messageIndex, messages }) => {
  const messageDate = utils.timestampToDate(timestamp);

  function shouldRender() {
    const previousMessage = messages[messageIndex - 1];
    return !previousMessage || (messageDate !== utils.timestampToDate(previousMessage.timestamp));
  }

  return shouldRender() && (
    <div className="chat__row chat__row--date">
      <p>{messageDate}</p>
    </div>
  );
};

Date.propTypes = {
  timestamp: PropTypes.string.isRequired,
  messageIndex: PropTypes.number.isRequired,
  messages: PropTypes.arrayOf(MessageType).isRequired,
};

export default Date;
