import React from 'react';
import PropTypes from 'prop-types';
import DownArrows from '../../resources/svg/down-arrow.svg';

const ChatHeader = ({ unreadMessagesCount, firstUnreadRef }) => {
  function scrollToFirstUnreadMessage() {
    window.scrollTo({
      top: firstUnreadRef.current.offsetTop,
      behavior: 'smooth',
    });
  }

  const getText = () => unreadMessagesCount > 0
    ? `You have ${unreadMessagesCount} unread messages.`
    : 'All messages read';

  window.title = `Chat App${unreadMessagesCount > 0 ? ` (${unreadMessagesCount})` : ''}`;

  return (
    <div className="chat-header">
      <p className="chat-header__text">{getText()}</p>
      {unreadMessagesCount > 0
      && <button
          className="chat-header__button"
          type="button"
          data-testid="scroll-down-button"
          onClick={scrollToFirstUnreadMessage}>
            <DownArrows />
        </button>}
    </div>
  );
};

ChatHeader.defaultProps = {
  firstUnreadRef: null,
};

ChatHeader.propTypes = {
  unreadMessagesCount: PropTypes.number.isRequired,
  firstUnreadRef: PropTypes.any,
};

export default ChatHeader;
