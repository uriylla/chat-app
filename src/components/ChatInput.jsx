import React, { useState } from 'react';
import PropTypes from 'prop-types';

const ChatInput = ({ sendMessage }) => {
  const [inputText, setInputText] = useState('');

  function onSubmitHandler(e) {
    e.preventDefault();
    if (inputText !== '') {
      sendMessage({
        text: inputText,
        direction: 'out',
        timestamp: Date.now().toString(),
        status: 'sent',
      });
      setInputText('');
    }
  }

  return (
    <form onSubmit={onSubmitHandler} className="chat-input">
      <input
        label=""
        type="text"
        placeholder="Type a message..."
        onChange={(e) => setInputText(e.target.value)}
        value={inputText}
      />
      <button type="submit">SEND</button>
    </form>
  );
};

ChatInput.propTypes = {
  sendMessage: PropTypes.func.isRequired,
};

export default ChatInput;
