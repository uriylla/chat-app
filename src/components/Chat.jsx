import React, { useEffect, useState, useRef } from 'react';
import Message from './Message';
import ChatHeader from './ChatHeader';
import ChatInput from './ChatInput';
import Date from './Date';

import '../styles/styles.scss';

const Chat = () => {
  const [messages, setMessages] = useState([]);
  const firstUnreadRef = useRef(null);

  useEffect(() => {
    fetch('http://localhost:3000/messages')
      .then((res) => res.json())
      .then((data) => setMessages(data));
  }, []);

  function countUnreadMessages() {
    return messages.filter((message) => message.direction === 'in' && message.status !== 'read').length;
  }

  function getFirstUnreadMessage() {
    return messages.find((msg) => msg.direction === 'in' && msg.status !== 'read');
  }

  function markMessageAsRead(id) {
    const msgIndex = messages.indexOf(messages.find((msg) => msg.id === id));
    setMessages(messages.map((message, i) => ({
      ...message,
      status: (i <= msgIndex && message.direction === 'in') ? 'read' : message.status,
    })));
  }

  function sendMessage(message) {
    const id = messages.length > 0 && messages[messages.length - 1].id + 1;
    setMessages([...messages, { ...message, id }]);
  }

  return (
    <div className="chat">
      <ChatHeader unreadMessagesCount={countUnreadMessages()} firstUnreadRef={firstUnreadRef} />
      <div className="chat__content">
        {messages.map((message, i) => (
          <div key={`message-${message.id}`}>
            <Date
              timestamp={message.timestamp}
              messageIndex={i}
              messages={messages}
            />
            <Message
              ref={getFirstUnreadMessage() === message ? firstUnreadRef : null}
              markMessageAsRead={markMessageAsRead}
              message={message}
            />
          </div>
        ))}
      </div>
      <ChatInput sendMessage={sendMessage} />
    </div>
  );
};

export default Chat;
