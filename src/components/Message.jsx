import React from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';
import MessageStatus, { statusType } from './MessageStatus';

const Message = React.forwardRef(({
  message: {
    text, direction, status, timestamp, id,
  }, markMessageAsRead,
}, ref) => {
  const date = new Date(parseInt(timestamp, 10));

  function onVisibilityChange(isVisible) {
    return isVisible && direction === 'in' && status !== 'read' && markMessageAsRead(id);
  }

  return (
    <div className={`chat__row chat__row--message-${direction}`}>
      <VisibilitySensor onChange={onVisibilityChange}>
        <div className={`message message--${direction}`} ref={ref}>
          <p className="message__text">{text}</p>
          <div className="message__info">
            <p className="message__time">{`${date.getHours()}:${date.getMinutes()}`}</p>
            {direction !== 'in'
              && <MessageStatus status={status} />}
          </div>
        </div>
      </VisibilitySensor>
    </div>
  );
});

export const MessageType = PropTypes.shape({
  text: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  direction: PropTypes.oneOf(['in', 'out']).isRequired,
  status: statusType.isRequired,
  timestamp: PropTypes.string.isRequired,
});

Message.propTypes = {
  message: MessageType.isRequired,
  markMessageAsRead: PropTypes.func.isRequired,
};

export default Message;
