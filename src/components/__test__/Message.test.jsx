import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom'
import Message from '../Message';
import messages from './testData';

describe('Message', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Message message={messages[0]} markMessageAsRead={jest.fn()} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
