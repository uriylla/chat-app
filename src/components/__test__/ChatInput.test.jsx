import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom'
import ChatInput from '../ChatInput';

describe('ChatInput', () => {
  test('should submit message correctly', () => {
    const sendMessageSpy = jest.fn();
    const inputText = 'test';

    render(<ChatInput sendMessage={sendMessageSpy} />);

    fireEvent.change(screen.getByRole("textbox"), {
      target: {value: inputText},
    });
    fireEvent.submit(screen.getByRole("button"));

    expect(sendMessageSpy).toHaveBeenCalledWith(expect.objectContaining({
      text: inputText,
      direction: 'out',
      status: 'sent',
    }));

    expect(screen.getByRole("textbox")).toBeEmptyDOMElement();
  });
});
