import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom'
import ChatHeader from '../ChatHeader';

describe('ChatHeader', () => {

  let unreadMessagesCount;
  const firstUnreadRefMock = { current: { offsetTop: 10 }};
  describe('when message count is 0', () => {

    beforeEach(() => {
      unreadMessagesCount = 0;
      render(<ChatHeader
        unreadMessagesCount={unreadMessagesCount}
        firstUnreadRef={firstUnreadRefMock}/>
      );
    });

    test('should render \'All messages read\'', () => {
      expect(screen.getByText('All messages read')).toBeInTheDocument();
    });
    
    test('should not render scroll down button', () => {
      expect(screen.queryByTestId(/scroll-down-button/i)).not.toBeInTheDocument();
    });
  });

  describe('when message count is greater than 0', () => {

    beforeEach(() => {
      unreadMessagesCount = 3;
      render(<ChatHeader
        unreadMessagesCount={unreadMessagesCount}
        firstUnreadRef={firstUnreadRefMock}/>
      );
    });

    test('should render \'Should display correct amount of unread messages\'', () => {
      expect(screen.getByText('You have 3 unread messages.')).toBeInTheDocument();
    });

    test('should render scroll down button', () => {
      expect(screen.queryByTestId(/scroll-down-button/i)).toBeInTheDocument();
    });

    describe('when user clicks scroll down button', () => {
      let windowScrollToAux;
      let windowScrollToSpy;

      beforeEach(() => {
        windowScrollToSpy = jest.fn();
        windowScrollToAux = window.scrollTo;
        window.scrollTo = windowScrollToSpy;
        fireEvent.click(screen.queryByTestId(/scroll-down-button/i));
      })

      afterEach(() => {
        window.scrollTo = windowScrollToAux;
      })

      test('should call window scrollTo correclty', () => {
        expect(windowScrollToSpy).toHaveBeenCalledWith({
          top: 10,
          behavior: 'smooth',
        });
      });
    });
  });

});
