export default [
  {
    "id": 1,
    "direction": "in",
    "status": "read",
    "timestamp": "1597834102",
    "text": "Alice was beginning"
  },
  {
    "id": 2,
    "direction": "out",
    "status": "received",
    "timestamp": "1627834168",
    "text": "to get very tired"
  },
  {
    "id": 3,
    "direction": "in",
    "status": "read",
    "timestamp": "1627834168",
    "text": "of sitting by her"
  },
  {
    "id": 4,
    "direction": "in",
    "status": "read",
    "timestamp": "1827834168",
    "text": "sister on the"
  },
];
