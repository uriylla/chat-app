import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'
import Date from '../Date';
import messages from './testData';

describe('Date', () => {
  let message;
  describe('when is the first message', () => {
    beforeEach(() => {
      message = messages[0];
      render(<Date
        timestamp={message.timestamp}
        messageIndex={0}
        messages={messages}
      />);
    });
  
    test('should render date', () => {
      expect(screen.getByText('19/1/1970')).toBeInTheDocument();
    })
  });

  describe('when date did not change', () => {
  
    test('should not render date', () => {
      message = messages[2];
      const { container } = render(<Date
        timestamp={message.timestamp}
        messageIndex={2}
        messages={messages}
      />);

      expect(container).toBeEmptyDOMElement();
    })
  });

  describe('when date did change', () => {
    test('should render date', () => {
      message = messages[3];
      render(<Date
        timestamp={message.timestamp}
        messageIndex={3}
        messages={messages}
      />);

      expect(screen.getByText('22/1/1970')).toBeInTheDocument();
    })
  });

  describe('if date is today', () => {
    test('should render \'Today\'', () => {
      message = messages[3];
      message.timestamp = global.Date.now().toString();
      render(<Date
        timestamp={message.timestamp}
        messageIndex={3}
        messages={messages}
      />);

      expect(screen.getByText('Today')).toBeInTheDocument();
    });
  });
});