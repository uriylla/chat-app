import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom'
import MessageStatus from '../MessageStatus';

describe('Message', () => {
  describe('when is sent', () => {
    test('should render correctly', () => {
      const { asFragment } = render(<MessageStatus status="sent" />);
      expect(asFragment()).toMatchSnapshot();
    });
  });
  describe('when is reveived', () => {
    test('should render correctly', () => {
      const { asFragment } = render(<MessageStatus status="received" />);
      expect(asFragment()).toMatchSnapshot();
    });
  });
  describe('when is read', () => {
    test('should render correctly', () => {
      const { asFragment } = render(<MessageStatus status="read" />);
      expect(asFragment()).toMatchSnapshot();
    });
  });
});
